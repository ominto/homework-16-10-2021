# Дана строка python best language. Проверить с помощью регулярных выражений найти с какого по какой символ встречается слово python
import re
string = 'python best language'
pattern = 'python'
result = re.match(pattern, string)
print(f'Слово python начинается с индекса {result.start()} и заканчивается индексом {result.end()}')

# Дана произвольная строка. Проверить встречается ли в ней дата время формата чч.мм.сс.
time_string = input('Введите строку:\n')
print('Встречается ли в строке дата и время формата чч.мм.сс: ')
print(re.findall(r'\d\d[.]\d\d[.]\d\d', time_string))

# Дана произвольная строка. Найти и вывести все слова, после которых идет запятая и пробел(если данные слова присутствуют)
word_string = input('Введите строку:\n')
print('Все слова, после которых идёт запятая и пробел: ')
print(re.findall(r'\w+[,][ ]', word_string))